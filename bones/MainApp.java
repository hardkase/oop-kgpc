/*OOP CA 5
 * 2GD1:author @ Kayleigh Grumley & Patrick Collins
 * Code heavily referenced NMCG's sample code, 
 * Thanks, Niall!
 * Version 0.5 - Getting Started
 * Bitbucket repo: https://hardkase@bitbucket.org/hardkase/oop-kgpc.git
 */

/*REFERENCE
 * data type sizes
 * BOOLEAN: 1 bit (0 or 1)
 * BYTE: 1 BYTE, 8 bits (127:-128)
 * SHORT: 2 BYTES, 16 bits (32767:-32768)
 * INT: 4 BYTES, 32 bits (2Billion+-)
 * LONG: 8 BYTES, 64 bits(9Quintillion+-int)
 * FLOAT: 4 BYTES, 32 bits(not precise)
 * DOUBLE: 8 BYTES, 64 bits(precise)
 * CHAR: 2 BYTES, 16 bit (i.e. 'a')
 */
public class MainApp
{
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}
	public void start()
	{
		
	}
}

