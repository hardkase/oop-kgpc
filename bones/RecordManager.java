//Handles File, Path, and FileNames
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RecordManager
{
	private String strPath, strName, strPermission;
	private File file;
	private RandomAccessFile raf;
	
	public RecordManager(String strPath, String strName, String strPermission)
	{
		this.setPath(strPath);
		this.setName(strName);
		this.setStringPermission(strPermission);
		File folder = new File (this.strPath);
		if (!folder.exists())
			folder.mkdirs();
	}
	private void setPath(String strPath)
	{
		this.strPath = validate(strPath, AppData.RegexFileName, AppData.DefaultFilePath );
	}
	private void setName(String strName)
	{
		this.strName = validate(strName, AppData.RegexFileName, AppData.DefaultFileName );
	}
	private void setPermission(String strPermission)
	{
		this.strPermission = validate(strPermission, Appdata.RegexFileName, Appdata.DefaultFilePermission);
	}
	public String getStrPath()
	{
		return strPath;
	}
	public String getStrName()
	{
		return strName;
	}
	public String getStrPermission()
	{
		return strPermission;
	}
	public static String validate(String strData, String strRegex, String strDefault )
	{
		strData = strData.toLowerCase();
		if (strData.matches(strRegex))
			return strData;
		else
			return strDefault;
	}
	public boolean open(boolean bAppend)
	{
		this.file = new File(this.strPath + this.strName);
		if(!bAppend)
			this.file.delete();
		try
		{
			this.raf=new RandomAccessFile(this.file, this.strPermission);
		}
		catch (FileNotFoundException e)
		{
			return false;
		}
		return false;
	}
	public boolean close()
	{
		try
		{
			this.raf.close();
		}
		catch (IOException e)
		{
			return false;
		}
		this.raf = null;
		this.file = null;
		return true;
	}
	public void write(short data)
	{
		write(-1, data);
	}
	public void write(int index, short data)
	{
		setPointer(index);
		try
		{
			this.raf.writeShort(data);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public short read(int index)
	{
		if((index<0) || (>= size()))
			return -1;
		setPointer(index)
		short data = -1;
		try
		{
			data = this.raf.readShort();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return data;
	}
	public void setPointer(int index)
	{
		try
		{
			int recordSize=2; //hardcoded, probably not a good thing<inflexible>
			if(index == 0)
			{
				this.raf.seek(0);
			}
			else if ((index >0) && (index < size()))
				this.raf.seek(index*recordSize);
			else
			{
				this.raf.seek(this.raf.length());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public int size()
	{
		int recordSize = 2; //(hardcoded?? - can I use Size from record???)
		int size = 0;
		try
		{
			size = (int)(this.raf.length()/recordSize);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return size;
	}
	// ADD print, find, edit, delete, deleteAll..:-)
}