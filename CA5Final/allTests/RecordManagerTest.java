import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import org.junit.Test;

public class RecordManagerTest {

	@Test
	public void testCreate() throws IOException {
		System.out.println();
		System.out.println("****Test Create*****");
		String strPath = "c:/temp/";
		String strName = "testCreate.txt";
		String strPer = "rw";
		boolean exists = RecordManager.ifExists(strPath, strName, strPer);
		assertTrue(!exists);
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		exists = RecordManager.ifExists(strPath, strName, strPer);

		assertTrue(exists);
	}

	@Test
	public void testCreate_notFound() throws IOException {
		System.out.println();
		System.out.println("****Test Create Not Found*****");
		String strPath = "c:/temp/";
		String strName = "testCreateNotFound.txt";
		String strPer = "rw";
		boolean exists = RecordManager.ifExists(strPath, strName, strPer);
		assertTrue(!exists);
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		exists = RecordManager.ifExists(strPath, strName, strPer);

		assertTrue(exists);
	}

	@Test
	public void testCreateUnique() throws IOException {
		System.out.println();
		System.out.println("****Test Create Unique*****");
		String strPath = "c:/temp/";
		String strName = "testCreateUnique.txt";
		String strPer = "rw";

		RecordManager l = new RecordManager(strPath, strName, strPer);
		l.open(true);
		boolean exists = RecordManager.ifExists(strPath, strName, strPer);
		assertTrue(exists);
		while (exists) {
			strName = "yayuh.txt";
			exists = RecordManager.ifExists(strPath, strName, strPer);
			assertTrue(!exists);
		}
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		exists = RecordManager.ifExists(strPath, strName, strPer);
		assertTrue(exists);
	}

	@Test
	public void testDelete() throws IOException {
		System.out.println();
		System.out.println("****Test Delete*****");
		RandomAccessFile raf = new RandomAccessFile("c:/temp/testDelete1.txt", "rw");

		Record r = new Record(3, 5, "Blondie", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true);
		Record l = new Record(3, 5, "Blondie", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true);

		assertNotNull(r);
		boolean success = r.write(raf);
		l.write(raf);
		assertTrue(success);
		assertEquals(raf.length(), Record.Size * 2);
		raf.setLength(raf.length() - Record.Size);
		assertEquals(raf.length(), Record.Size);
		String strPath = "c:/temp/";
		String strName = "testDelete2.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.write(new Record(2, 5, "fdgsds", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(6, 5, "ggg", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		assertEquals(rm.size(), 3);
		rm.deleteAtPoint(1);
		assertEquals(rm.size(), 2);
		rm.close();
	}

	@Test
	public void testDelete_NotFound() throws IOException {
		System.out.println();
		System.out.println("****Test Delete Not Found*****");
		String strPath = "c:/temp/";
		String strName = "testDeleteNotFound.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.write(new Record(2, 5, "fdgsds", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(6, 5, "ggg", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		assertEquals(rm.size(), 3);
		rm.deleteAtPoint(5);
		assertEquals(rm.size(), 3);
		rm.close();
	}

	@Test
	public void testAdd() throws IOException {
		System.out.println();
		System.out.println("****Test Add*****");
		String strPath = "c:/temp/";
		String strName = "testDeleteNotFound.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		ArrayList<Record> out = rm.filterByString("sdgsd", 1);
		assertTrue(out.size() > 0);
	}

	@Test
	public void testEdit() throws IOException {
		System.out.println();
		System.out.println("****Test Edit*****");
		String strPath = "c:/temp/";
		String strName = "testEdit.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.edit(0, 1);
		// at inout please write lol
		ArrayList<Record> out = rm.filterByString("lol", 1);
		assertTrue(out.size() > 0);
	}

	@Test
	public void testEdit_NotFound() throws IOException {
		System.out.println();
		System.out.println("****Test edit Not Found*****");
		String strPath = "c:/temp/";
		String strName = "testEditNotFound.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.edit(3, 1);
		// at inout please write lol
		ArrayList<Record> out = rm.filterByString("lol", 1);
		assertTrue(out.size() == 0);
	}

	@Test
	public void testSearchByCriteria() throws IOException {
		System.out.println();
		System.out.println("****Test Search By*****");
		String strPath = "c:/temp/";
		String strName = "testsearchCriteria.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(4, 7, "sdgsd", "idgggek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(2, 9, "sdgsd", "idgggek", (byte) 2, (byte) 4, (byte) 70, 1972, true), -1);

		System.out.println();
		System.out.println("Searching Artist name ");
		ArrayList<Record> out = rm.filterByString("sdgsd", 1);
		assertTrue(out.size() > 0);
		for (Record r : out) {
			System.out.println(r.toString());
		}
		System.out.println();
		System.out.println("Searching genre");
		ArrayList<Record> out2 = rm.filterByByte((byte) 2, 3);
		assertTrue(out2.size() > 0);
		for (Record r : out2) {
			System.out.println(r.toString());
		}
	}

	@Test
	public void testSearchByCriteria_NotFound() throws IOException {
		System.out.println();
		System.out.println("****Test Search By not Found*****");
		String strPath = "c:/temp/";
		String strName = "testsearchCriteriaNotFound.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(4, 7, "sdgsd", "idgggek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(2, 9, "sdgsd", "idgggek", (byte) 2, (byte) 4, (byte) 70, 1972, true), -1);
		System.out.println("Not Finding Anything");
		System.out.println();
		ArrayList<Record> out = rm.filterByString("ghgf", 1);
		assertTrue(out.size() == 0);
		for (Record r : out) {
			System.out.println(r.toString());
		}
		System.out.println();
		ArrayList<Record> out2 = rm.filterByByte((byte) 7, 3);
		assertTrue(out2.size() == 0);
		for (Record r : out2) {
			System.out.println(r.toString());
		}
	}
	@Test
	public void testDeleteAndSearch() throws IOException {
		System.out.println();
		System.out.println("****Test Delete and Search*****");
		String strPath = "c:/temp/";
		String strName = "testDeleteAndSearch.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.write(new Record(2, 5, "fdgsds", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(6, 5, "ggg", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		assertEquals(rm.size(), 3);
		System.out.println("Before delete finding fdgsds");
		ArrayList<Record> out = rm.filterByString("fdgsds", 1);
		assertTrue(out.size() > 0);
		for (Record r : out) {
			System.out.println(r.toString());
		}
		rm.deleteAtPoint(1);
		assertEquals(rm.size(), 2);
		System.out.println("After delete not finding fdgsds");
		ArrayList<Record> out2 = rm.filterByString("fdgsds", 1);
		assertTrue(out2.size() == 0);
		for (Record r : out2) {
			System.out.println(r.toString());
		}
		rm.close();
	}

	@Test
	public void testOpen() throws IOException {
		System.out.println();
		System.out.println("****Test Open*****");
		String strPath = "c:/temp/";
		String strName = "testOpen.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);

		boolean exists = RecordManager.ifExists(strPath, strName, strPer);

		exists = RecordManager.ifExists(strPath, strName, strPer);

		assertTrue(exists);
	}
	@Test
	public void testDeleteAll() throws IOException {
		System.out.println();
		System.out.println("****Test Delete All*****");
		String strPath = "c:/temp/";
		String strName = "testDeleteAndSearch.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.write(new Record(2, 5, "fdgsds", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(6, 5, "ggg", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		assertEquals(rm.size(), 3);
		rm.deleteAllRecords();
		assertEquals(rm.size(), 0);
		assertEquals(rm.countRecords(), 0);
	}
	@Test
	public void testShowAll() throws IOException {
		System.out.println();
		System.out.println("****Test Show All*****");
		String strPath = "c:/temp/";
		String strName = "testShowAll.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		rm.write(new Record(6, 8, "sdgsd", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), 0);
		rm.write(new Record(2, 5, "fdgsds", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		rm.write(new Record(6, 5, "ggg", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true), -1);
		assertEquals(rm.size(), 3);
		System.out.println("Show all records");
		rm.close();
		rm.open(true);
		rm.print();
		rm.close();
	}
	@Test
	public void testShowAll_NotFound() throws IOException {
		System.out.println();
		System.out.println("****Test Show All Not found*****");
		String strPath = "c:/temp/";
		String strName = "testShowAllNotFound.txt";
		String strPer = "rw";
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		assertEquals(rm.size(), 0);
		
		rm.close();
		System.out.println("Show all records");
		rm.open(true);
		rm.print();
		rm.close();
	}
	@Test
	public void testOpen_NotFound() throws IOException {
		System.out.println();
		System.out.println("****Test Open Not found*****");
		String strPath = "c:/temp/";
		String strName = "dfgdfgd.txt";
		String strPer = "rw";

		boolean exists = RecordManager.ifExists(strPath, strName, strPer);
		assertTrue(!exists);
		while (!exists) {
			strName = "testOpen.txt";
			exists = RecordManager.ifExists(strPath, strName, strPer);
		}
		assertTrue(exists);
	}

	@Test
	public void testAddRecord() throws IOException {
		System.out.println();
		System.out.println("****Test Add Record*****");
		RandomAccessFile raf = new RandomAccessFile("c:/temp/testWrite.txt", "rw");

		Record r = new Record(3, 5, "Blondie", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true);

		assertNotNull(r);
		boolean success = r.write(raf);
		assertTrue(success);
		boolean exists = new File("c:/temp/testWrite.txt").exists();
		assertTrue(exists);
		long length = raf.length();
		assertEquals(length, Record.Size);
		raf.close();
	}

	@Test
	public void testReadRecord() throws IOException {
		System.out.println();
		System.out.println("****Test ReadRecord*****");
		RandomAccessFile raf = new RandomAccessFile("c:/temp/testRead.txt", "rw");

		Record r = new Record(3, 5, "Blondie", "idek", (byte) 3, (byte) 4, (byte) 70, 1972, true);

		assertNotNull(r);
		boolean success = r.write(raf);
		assertTrue(success);
		raf.seek(0);
		Record l = Record.read(raf);
		assertEquals(l.getAlbumId(), 3);
		assertEquals(l.getArtistId(), 5);
		assertEquals(l.getArtistName(), "Blondie");
		assertEquals(l.getAlbumName(), "idek");
		assertEquals(l.getpMusicType(), 3);
		assertEquals(l.getsMusicType(), 4);
		assertEquals(l.getEra(), 70);
		assertEquals(l.getReleaseDate(), 1972);
		assertEquals(l.isClassic(), true);
		raf.close();
	}
}
