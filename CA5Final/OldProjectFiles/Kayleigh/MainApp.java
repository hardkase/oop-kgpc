import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


/*OOP CA 5
 * 2GD1:author @ Kayleigh Grumley & Patrick Collins
 * Code heavily referenced NMCG's sample code, 
 * Thanks, Niall!
 * Version 0.5 - Getting Started
 * Bitbucket repo: https://hardkase@bitbucket.org/hardkase/oop-kgpc.git
 */

/*REFERENCE
 * data type sizes
 * BOOLEAN: 1 bit (0 or 1)
 * BYTE: 1 BYTE, 8 bits (127:-128)
 * SHORT: 2 BYTES, 16 bits (32767:-32768)
 * INT: 4 BYTES, 32 bits (2Billion+-)
 * LONG: 8 BYTES, 64 bits(9Quintillion+-int)
 * FLOAT: 4 BYTES, 32 bits(not precise)
 * DOUBLE: 8 BYTES, 64 bits(precise)
 * CHAR: 2 BYTES, 16 bit (i.e. 'a')
 */

public class MainApp {
	String strPath ;
	String strName ;
	String strPer ;

	public static void main(String[] args) {
		MainApp theApp = new MainApp();

		try {
			theApp.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void start() throws IOException {
		 strPath = "c:/temp/";
		 strName = "ca51.txt";
		 strPer = "rw";
		//createDatabase();
		openDatabase();
		//addRecord(strPath, strName, strPer);
		System.out.println();
		printRecords(strPath, strName, strPer);
		System.out.println();
		// printFromIndex(strPath, strName, strPer);
		//System.out.println();
		// deleteRecordFromPos(strPath, strName, strPer);
		//System.out.println();
		// printRecords(strPath, strName, strPer);
		//System.out.println();
		// editRecord(strPath, strName, strPer);
		//System.out.println();
		// countrecords(strPath, strName, strPer);
		//System.out.println();
		// deleteAllRecords(strPath, strName, strPer);
		//System.out.println();
		searchBy(strPath, strName, strPer);
	}
//OPEN DB
	private void openDatabase() throws FileNotFoundException {
		 strPath = "c:/temp/";
		 String name = ScannerUtility.getString("Please enter name of database you would like to open : ");
		 strName=name+".txt";
		 strPer = "rw";	
		 boolean exists=RecordManager.ifExists(strPath, strName, strPer);
			while(!exists){
				 strName = ScannerUtility.getString("Database Doesn't exist! Please enter name of database you would like to open : ");
				 strName=name+".txt";
				 exists=RecordManager.ifExists(strPath, strName, strPer);
			}
	}
//CREATE DB
	private void createDatabase() throws FileNotFoundException {
		 strPath = "c:/temp/";
		 String name = ScannerUtility.getString("Please enter name of database you would like to create : ");
		 strName=name+".txt";
		 strPer = "rw";	
		boolean exists=RecordManager.ifExists(strPath, strName, strPer);
		while(exists){
			 strName = ScannerUtility.getString("Database already exists! Please enter name of database you would like to create : ");
			 strName=name+".txt";
			 exists=RecordManager.ifExists(strPath, strName, strPer);
		}
		
	}
//SEARCH BY
	private void searchBy(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		System.out.println();
		System.out.println(
				"Artist ID = 0, Artist Name = 1, Album name = 2, Genre music = 3, Era = 4, Released Date = 5, Classic or not = 6");
		int search = ScannerUtility.getInt("What would You like to search by? ");
		if (search == 1 || search == 2) {

			String name = ScannerUtility.getString("Please enter what you would like to search for :");
			ArrayList<Record> filteredStringList = rm.filterByString(name,search);
			printAray(filteredStringList);
		}
		else if(search == 3 || search == 4){
			if(search==3){
				System.out.println("Hip Hop =1, Rock = 2, Indie = 3, R&B = 4, Metal = 5, KPOP = 6, Jazz = 7, No Genre = 0");
			}
			byte genreOrEra = ScannerUtility.getByte("Please enter what you would like to search for :");
			ArrayList<Record> filteredStringList = rm.filterByByte(genreOrEra,search);
			printAray(filteredStringList);
		}
		else if (search == 0 || search == 5) {

			int iDorReleaseDate = ScannerUtility.getInt("Please enter what you would like to search for :");
			ArrayList<Record> filteredStringList = rm.filterByInt(iDorReleaseDate,search);
			printAray(filteredStringList);
		}
		rm.close();
	}
//PRINT FILTERED ARRAYLIST OF RECs
	private void printArray(ArrayList<Record> filteredStringList) {
		for (Record r : filteredStringList) {
			System.out.println(r.toString());
		}
	}
//DELETE ALL 
	private void deleteAllRecords(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		int yousure = ScannerUtility.getInt("Are you sure you wish to delete all records? yes=1, no=2 ");
		if (yousure == 1) {
			rm.deleteAllRecords();
		}

		rm.close();
	}
//COUNT RECs
	private void countrecords(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		int count = rm.countRecords();
		System.out.println("There are " + count + " Records in this database.");
		rm.close();
	}
//EDIT REC
	private void editRecord(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		int index = ScannerUtility.getInt("please enter position of record you would like to edit ");
		rm.print(index);
		System.out.println(
				"Artist Name = 1, Album name = 2, Primary music = 3, Secondary Music = 4, Era = 5, Released Date = 6, Classic or not = 7");
		int edit = ScannerUtility.getInt("What would you like to change? ");
		while (edit > 7) {
			edit = ScannerUtility.getInt("Please pick between 1-9. What would you like to change? ");
		}
		rm.edit(index, edit);
		rm.close();
	}
//DELETE FROM INDEX n
	private void deleteRecordFromPos(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		int index = ScannerUtility.getInt("please enter position of record you would like to delete ");
		rm.deleteAtPoint(index);
		rm.close();
	}
//PRINT FROM INDEX n
	private void printFromIndex(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		int index = ScannerUtility.getInt("please enter position of record you would like to view ");

		rm.print(index);
		rm.close();
	}
//CREATE REC
	private void addRecord(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);
		rm.open(true);
		int lastAlbumId = rm.getLastRecordAlbumId();
		int albumid = lastAlbumId + 1;
		int artistid = ScannerUtility.getInt("Please enter Artists id : ");
		String artist = ScannerUtility.getString("Please enter artist name");
		String titleAlbum = ScannerUtility.getString("Please enter album name ");
		System.out.println("Hip Hop =1, Rock = 2, Indie = 3, R&B = 4, Metal = 5, KPOP = 6, Jazz = 7, No Genre = 0");
		byte pmusic = ScannerUtility.getByte("Please enter primary genre :");
		byte smusic = ScannerUtility.getByte("Please enter secondary genre if any :");
		byte era = ScannerUtility
				.getByte("Please enter Era of music e.g. 90's,80's etc. Note! only put in the number e.g. 90 ");
		int release = ScannerUtility.getInt("Please enter release date ");
		String classic = ScannerUtility.getString("is this album a classic? Please type yes/no ");
		boolean classicB;
		if (classic.equals("yes")) {
			classicB = true;
		} else {
			classicB = false;
		}
		rm.write(new Record(albumid, artistid, artist, titleAlbum, pmusic, smusic, era, release, classicB), -1);

		System.out.println("Record size after addition : " + rm.size());
		rm.close();
	}
//PRINT REC
	private void printRecords(String strPath, String strName, String strPer) throws IOException {
		RecordManager rm = new RecordManager(strPath, strName, strPer);

		System.out.println("print and read");
		rm.open(true);
		rm.print();
		rm.close();
	}
}
