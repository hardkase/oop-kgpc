//Pre-Define formats for strings and other data
//would be really cool to take blase user data and conform to nifty string formats
//like: <user>: prince and the new power generation
//formatted: Prince and the New Power Generation
public class AppData
{
	public static final String DefaultDriveLetter = System.getenv("SystemDrive"); //e.g. "c"
	
	public static final String DefaultFilePath = DefaultDriveLetter + "/temp/";
	
	public static final String RegexFilePath = "([a-zA-Z]:(/|\\\\)){1}([a-zA-Z0-9_-]{1,}(/|\\\\)){0,}";
	
	public static final String RegexFilePermission = "(r|rw|rws|rwd)";
	
	public static final String DefaultFilePermission = "rw";

	public static final String RegexFileName = "[a-zA-Z]{1,}[a-zA-Z0-9]{0,}\\.[a-zA-Z]{1,4}";

	public static final String DefaultFileName = "myData.txt";

}
