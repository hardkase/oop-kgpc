
//Handles File, Path, and FileNames
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class RecordManager {
	private String strPath, strName, strPermission;
	private File file;
	private RandomAccessFile raf;
//CONSTRUCTOR
	public RecordManager(String strPath, String strName, String strPermission) {
		this.setPath(strPath);
		this.setName(strName);
		this.setPermission(strPermission);
		File folder = new File(this.strPath);
		if (!folder.exists())
			folder.mkdirs();
	}
//Gs&Ss
	private void setPath(String strPath) {
		this.strPath = validate(strPath, AppData.RegexFileName, AppData.DefaultFilePath);
	}

	private void setName(String strName) {
		this.strName = validate(strName, AppData.RegexFileName, AppData.DefaultFileName);
	}

	private void setPermission(String strPermission) {
		this.strPermission = validate(strPermission, AppData.RegexFilePermission, AppData.DefaultFilePermission);
	}

	public String getStrPath() {
		return strPath;
	}

	public String getStrName() {
		return strName;
	}

	public String getStrPermission() {
		return strPermission;
	}
//VALIDATE
	public static String validate(String strData, String strRegex, String strDefault) {
		strData = strData.toLowerCase();
		if (strData.matches(strRegex))
			return strData;
		else
			return strDefault;
	}
//OPEN
	public boolean open(boolean bAppend) {
		this.file = new File(this.strPath + this.strName);
		if (!bAppend)
			this.file.delete();
		try {
			this.raf = new RandomAccessFile(this.file, this.strPermission);
		} catch (FileNotFoundException e) {
			return false;
		}
		return false;
	}
//CLOSE
	public boolean close() {
		try {
			this.raf.close();
		} catch (IOException e) {
			return false;
		}
		this.raf = null;
		this.file = null;
		return true;
	}
//RM WRITE
	public void write(Record r, int index) {
		setPointer(index);
		try {
			r.write(this.raf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
//class examples
	public void write(short data) {
		write(-1, data);
	}

	public void write(int index, short data) {
		setPointer(index);
		try {
			this.raf.writeShort(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//RM READ
	public Record read() throws IOException {
		return Record.read(this.raf);
	}

	public Record read(int index) {
		if (index < 0 || index >= size())
			return null;
		setPointer(index);
		Record r = null;
		try {
			r = read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return r;
	}
//SET POINTER
	public void setPointer(int index) {
		try {
			int recordSize = Record.Size; // hardcoded, probably not a good
			// thing<inflexible>
			if (index == 0) {
				this.raf.seek(0);
			} else if ((index > 0) && (index < size()))
				this.raf.seek(index * recordSize);
			else {
				this.raf.seek(this.raf.length());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//SIZE
	public int size() {
		int recordSize = Record.Size; // (hardcoded?? - can I use Size from
										// record???)
		int size = 0;
		try {
			size = (int) (this.raf.length() / recordSize);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return size;
	}
//RM PRINT
	public void print() throws IOException {
		int recoirdSize = this.size();
		for (int i = 0; i < recoirdSize; i++) {
			Record r = null;
			r = read();
			System.out.println(r);
		}
	}
//RM PRINT INDEX n
	public void print(int index) throws IOException {

		Record r = null;
		r = read(index);
		System.out.println(r);

		// ADD print, find, edit, delete, deleteAll..:-)
	}
//DELETE AT INDEX n
	public void deleteAtPoint(int index) throws IOException {
		int recordSize = Record.Size;
		raf.seek(raf.length() - recordSize);
		Record last = read();

		write(last, index);
		raf.setLength(raf.length() - recordSize);

	}
//EDIT
	public void edit(int index, int edit) {
		Record r = null;
		r = read(index);

		int albumid = r.getAlbumId();
		int artistid = r.getArtistId();
		String artist = r.getArtistName();
		String titleAlbum = r.getAlbumName();
		byte pmusic = r.getpMusicType();
		byte smusic = r.getsMusicType();
		byte era = r.getEra();
		int release = r.getReleaseDate();
		boolean classicB = r.isClassic();

		if (edit == 0) {
			artistid = ScannerUtility.getInt("pleas enter Artists ID ");
		} else if (edit == 1) {
			artist = ScannerUtility.getString("please enter artist name");
		} else if (edit == 2) {
			titleAlbum = ScannerUtility.getString("please enter Album name ");
		} else if (edit == 3) {
			pmusic = ScannerUtility.getByte("please enter primary genre ");
		} else if (edit == 4) {
			smusic = ScannerUtility.getByte("please enter secondary genre ");
		} else if (edit == 5) {
			era = ScannerUtility.getByte("please enter genre of music ");
		} else if (edit == 6) {
			release = ScannerUtility.getInt("please enter release date ");
		} else if (edit == 7) {
			String classic = ScannerUtility.getString("is this album a classic? yes/no ");

			if (classic.equals("yes")) {
				classicB = true;
			} else {
				classicB = false;
			}
		}
		write(new Record(albumid, artistid, artist, titleAlbum, pmusic, smusic, era, release, classicB), index);

	}
//GET LAST RECORD
	public int getLastRecordAlbumId() throws IOException {
		int recordSize = Record.Size;
		if (raf.length() > 0) {
			raf.seek(raf.length() - recordSize);
			Record last = read();
			int lastAlbumid = last.getAlbumId();
			return lastAlbumid;
		} else {
			return -1;
		}
	}
//RECORDS COUNT
	public int countRecords() throws IOException {
		return size();
	}
//DELETE ALL
	public void deleteAllRecords() throws IOException {
		raf.setLength(0);
	}
//FIND BY ARTIST NAME - Needs explainin
	public ArrayList<Record> filterByString(String name, int search) throws IOException {
		ArrayList<Record> listOut = new ArrayList<Record>();

		for (int i = 0; i < size(); i++) {

			Record r = read(i);
			if (search == 1) {
				if (r.getArtistName().equalsIgnoreCase(name)) {
					listOut.add(r);
				}
			} else {
				if (r.getAlbumName().equalsIgnoreCase(name)) {
					listOut.add(r);
				}
			}
		}

		return listOut;
	}
//SEARCH BY MUSIC TYPE
	public ArrayList<Record> filterByByte(byte nByte, int search) {
		ArrayList<Record> listOut = new ArrayList<Record>();

		for (int i = 0; i < size(); i++) {

			Record r = read(i);
			if (search == 3) {
				
				if (r.getsMusicType() == nByte) {
					listOut.add(r);
				} else if (r.getpMusicType() == nByte) {
					listOut.add(r);
				}
			} else {
				if (r.getEra() == nByte) {
					listOut.add(r);
				}
			}
		}

		return listOut;
	}
//FILTER BY RELEASE DATE
	public ArrayList<Record> filterByInt(int iDorReleaseDate, int search) {
		ArrayList<Record> listOut = new ArrayList<Record>();

		for (int i = 0; i < size(); i++) {

			Record r = read(i);
			if (search == 0) {
				if (r.getArtistId()==iDorReleaseDate) {
					listOut.add(r);
				}
			} else {
				if (r.getReleaseDate()==iDorReleaseDate) {
					listOut.add(r);
				}
			}
		}

		return listOut;
	}
//EXISTS
	public static boolean ifExists(String strPath, String strName, String strPer) throws FileNotFoundException {
		File file=new File(strPath+strName);
		
		if(file.exists()){
			return true;
		}
		return false;
	}
}