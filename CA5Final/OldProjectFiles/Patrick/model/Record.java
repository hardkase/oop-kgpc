package model;
import java.io.IOException;
import utility.StringUtility;
import utility.AppData;
import java.io.RandomAccessFile;

import utility.StringUtility;
//changelog: removing sMusicType changing pMusicType to musicType, removing release date, adding userRating(int)
//This is our record class, in our case, for CDs (or albums in general...)
public class Record
{
	public static final int Size = 212;
	private int albumId;//all albums millions?
	private int artistId;//all artists millions
	private String artistName;//some long artist names
	//like: Orchestral Manoeuvres in the Dark 33 chars incl spaces
	//so like 100 bytes just in case, or too long?
	private String albumName;//crazy long albums (look up Fiona Apple
	//this we'll have to chop (in rare cases). Let's do 100 bytes
	private byte musicType;//primary music type: main genre
	//private byte sMusicType;//secondary music type: sub-genre
	private byte era;//id for like 50's, 60's, etc.
	/*private int releaseDate;will start as int, shine it up as date/long
	or drop and replace with simple int "rating"?*/
	private boolean isClassic;
	private byte userRating;
	//(does Size count? <+4>)4+4+100+100+1+1+1+4+1=216?
	public Record(int albumId, int artistId, String artistName, 
			String albumName, byte musicType, /*byte sMusicType,*/
			byte era, /*int releaseDate,*/ boolean isClassic, byte userRating)
	{
		this.albumId=albumId;
		this.artistId=artistId;
		this.artistName=artistName;
		this.albumName=albumName;
		this.musicType=musicType;
		//this.sMusicType=sMusicType;
		this.era=era;
		//this.releaseDate=releaseDate;
		this.isClassic=isClassic;
		this.userRating=userRating;
	}
/*	***Overload constructor - IDs will be padded serialized ints,
 * albumId will always be last used +1, artist ID could check for
 * artistName.exists() and return previous record's artist ID???
 * public Record(String artistName, 
			String albumName, byte pMusicType, byte sMusicType,
			byte era, int releaseDate, boolean isClassic)
	{
		//this.albumId=albumId; - function to get last album ID and +1 it
		//this.artistId=artistId; - check if artist exists in record, reuse ID
		this.artistName=artistName;
		this.albumName=albumName;
		this.pMusicType=pMusicType;
		this.sMusicType=sMusicType;
		this.era=era;
		this.releaseDate=releaseDate;
		this.isClassic=isClassic;
	}*/
	public int getAlbumId()
	{
		return albumId;
	}
	public void setAlbumId(int albumId)
	{
		this.albumId = albumId;
	}
	public int getArtistId()
	{
		return artistId;
	}
	public void setArtistId(int artistId)
	{
		this.artistId = artistId;
	}
	public String getArtistName()
	{
		return artistName;
	}
	public void setArtistName(String artistName)
	{
		this.artistName = artistName;
	}
	public String getAlbumName()
	{
		return albumName;
	}
	public void setAlbumName(String albumName)
	{
		this.albumName = albumName;
	}
	public byte getMusicType()
	{
		return musicType;
	}
	public void setMusicType(byte musicType)
	{
		this.musicType = musicType;
	}
	/*public byte getsMusicType()
	{
		return sMusicType;
	}
	public void setsMusicType(byte sMusicType)
	{
		this.sMusicType = sMusicType;
	}*/
	public byte getEra()
	{
		return era;
	}
	public void setEra(byte era)
	{
		this.era = era;
	}
	/*public int getReleaseDate()
	{
		return releaseDate;
	}
	public void setReleaseDate(int releaseDate)
	{
		this.releaseDate = releaseDate;
	}*/
	public boolean isClassic()
	{
		return isClassic;
	}
	public void setClassic(boolean isClassic)
	{
		this.isClassic = isClassic;
	}
	public byte getUserRating()
	{
		return userRating;
	}
	public void setUserRating()
	{
		this.userRating=userRating;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + albumId;
		result = prime * result + ((albumName == null) ? 0 : albumName.hashCode());
		result = prime * result + ((artistName == null) ? 0 : artistName.hashCode());
		result = prime * result + artistId;
		result = prime * result + era;
		result = prime * result + (isClassic ? 1231 : 1237);
		result = prime * result + musicType;
		result = prime * result + userRating;
		//result = prime * result + releaseDate;
		//result = prime * result + sMusicType;
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (albumId != other.albumId)
			return false;
		if (albumName == null) {
			if (other.albumName != null)
				return false;
		} else if (!albumName.equals(other.albumName))
			return false;
		if (artistName == null) {
			if (other.artistName != null)
				return false;
		} else if (!artistName.equals(other.artistName))
			return false;
		if (artistId != other.artistId)
			return false;
		if (era != other.era)
			return false;
		if (isClassic != other.isClassic)
			return false;
		if (musicType != other.musicType)
			return false;
		if (userRating != other.userRating)
			return false;
		/*if (releaseDate != other.releaseDate)
			return false;*/
		/*if (sMusicType != other.sMusicType)
			return false;*/
		return true;
	}
	
	/*
	 * (int albumId, int artistId, String artist, 
			String albumName, byte pMusicType, byte sMusicType,
			byte era, int releaseDate, boolean isClassic)
	 */
	//write method
	public boolean write(RandomAccessFile raf) throws IOException
	{
		try
		{
			raf.writeInt(this.getAlbumId());
			raf.writeInt(this.getArtistId());
			raf.writeUTF(StringUtility.pad(this.getArtistName(), StringUtility.StandardPadLength, StringUtility.PadString));
			raf.writeUTF(StringUtility.pad(this.getAlbumName(), StringUtility.StandardPadLength, StringUtility.PadString));
			raf.writeByte(this.getMusicType());
			raf.writeByte(this.getEra());
			raf.writeBoolean(this.isClassic);
			raf.writeByte(this.getUserRating());
			raf.close();
			//it's yellow, so we let it mellow aka flush later
			return true;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	public static Record read (RandomAccessFile raf) throws IOException
	{
		try
		{
		int albumId = raf.readInt();
		int artistId=raf.readInt();
		String artistName = StringUtility.unpad(raf.readUTF(), StringUtility.PadString);
		String albumName= StringUtility.unpad(raf.readUTF(), StringUtility.PadString);
		byte musicType= raf.readByte();
		//byte sMusicType= raf.readByte();
		byte era = raf.readByte();
		//int releaseDate = raf.readInt();
		boolean isClassic = raf.readBoolean();
		byte userRating = raf.readByte();
		return new Record(albumId, artistId, artistName, albumName, musicType,
				 era, isClassic, userRating);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	public static boolean search(RandomAccessFile raf) throws IOException
	{
		
		return false;
	}
}

