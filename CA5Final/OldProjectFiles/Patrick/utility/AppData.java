package utility;
/*Pre-Define formats for strings and other data
would be really cool to take blase user data and conform to nifty string formats
like: <user>: prince and the new power generation
<formatted>: Prince and the New Power Generation
* <u>sergeant pepper's lonely hearts club band
 * <u - date> 02052016
 * <f-date>02/05/2016 (
 */
public class AppData
{
public static final String DefaultDrive= System.getenv("SytemDrive");
public static final String DefaultPath = "";
public static final String RegexPath = "";
public static final String RegexFilePerm="";
public static final String DefaultFilePerm="rw";
public static final String RegexFileName="";
public static final String DefaultFileName="hozebag.txt";
public static final String DefaultPadString="*";
}
