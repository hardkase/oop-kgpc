package view;
import java.io.RandomAccessFile;
import utility.MenuHandler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import utility.StringUtility;
import utility.MenuHandler;
import utility.PrintHandler;
import utility.ScannerUtility;
import utility.AppData;
import controller.RecordManager;
import model.Record; 
/*OOP CA 5
 * 2GD1:author @ Kayleigh Grumley & Patrick Collins
 * Code heavily referenced NMCG's sample code, 
 * Thanks, Niall!
 * Version 0.5 - Getting Started
 * Bitbucket repo: 
 * 2GD1:author @ Kayleigh Grumley & Patrick Collins
 * Code heavily referenced NMCG's sample code, 
 * Thanks, Niall!
 * Version 0.6 - Base classes built out, AppData, Main, and Menu need to be built out, RecordManager
 * needs additional methods
 * OLDVersion 0.5 - Getting Started
 * Bitbucket repo: https://hardkase@bitbucket.org/hardkase/oop-kgpc.git
 */

/*REFERENCE
 * data type sizes
 * BOOLEAN: 1 bit (0 or 1)
 * BYTE: 1 BYTE, 8 bits (127:-128)
 * SHORT: 2 BYTES, 16 bits (32767:-32768)
 * INT: 4 BYTES, 32 bits (2Billion+-)
 * LONG: 8 BYTES, 64 bits(9Quintillion+-int)
 * FLOAT: 4 BYTES, 32 bits(not precise)
 * DOUBLE: 8 BYTES, 64 bits(precise)
 * CHAR: 2 BYTES, 16 bit (i.e. 'a')
 * Our Record:
 * Int + Int + (big) String + (big) String
 * +byte+byte+bool+byte
 * 4+4+100+100+1+1+1+1= 212 Bytes
 * RecordSize=212 Bytes
 * Intra Record Index
 * [0]=AlbumId [4]=ArtistId [8]=ArtistName
 * [108]=albumName [208]=musicType [209]=era 
 * [210]=isClassic[211]=userRating;
 */
public class MainApp
{
	public static void main(String[] args) throws IOException
	{
		MainApp theApp =new MainApp();
		theApp.start();
	}
private MenuHandler mainMenu, createMenu, searchMenu, deleteMenu, settingsMenu;
private String path = AppData.DefaultDrive+AppData.DefaultFileName;
private String fName = AppData.DefaultFileName;
private String perm = AppData.DefaultFilePerm;
private String padString = AppData.DefaultPadString;
	public void start() throws IOException
	{
		System.out.println("Welcome messagey thing");
		//
		startMenus();
		showMain();
	}
	private void showMain() throws IOException
	{
		int pick = 0; //choice
		do
		{
			pick = this.mainMenu.showMenuGetChoice("Pick one");
			if (pick ==1)//CREATE - probably doesnt need own menu options, just launch function
			{
				createNewRecord();//createRecord();
			}
			else if (pick==2)//Search + Edit/Delete
			{
				searchAndEdit();
			}
			else if (pick == 3)//Delete
			{
				deleteStuff();
			}
			else//Settings
			{
				editSettings();
			}
				
		}while(pick != this.mainMenu.getIndexOfExitOption());
		System.out.println("See you later!");
	}
	private void createNewRecord() throws FileNotFoundException
	{
		RecordManager rManager = new RecordManager(path, fName, perm );
		rManager.open(true);
		try
		{
			System.out.println("Create a new album record");
			int albumId = ScannerUtility.getInt("Enter the album ID: ");
			int artistId = ScannerUtility.getInt("Enter the artist ID: ");
			String artistName = ScannerUtility.getString("Artist Name: ");
			String albumName = ScannerUtility.getString("Album Name: ");
			byte musicType = ScannerUtility.getByte("Enter the Genre Code: ");
			byte era = ScannerUtility.getByte("Enter the Era Code: ");
			boolean isClassic = ScannerUtility.getBoolean("Is this album a Classic?");
			byte userRating = ScannerUtility.getByte("Rate album 1 - 5");
			File file = new File(strPath, strFName);
			RandomAccessFile raf = new RandomAccessFile("f:/temp/test5.txt", "rw");
			Record r1 = new Record(albumId, artistId, artistName, albumName, musicType, era, isClassic, userRating);
			long lgt = raf.length();
			r1.write(raf);
			raf.close();
			System.out.println("Record size: "+lgt);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	private void createMenu()
	{
		//create record method
		//not sure if this needs to be here, 
		//easier to just launch add record from main???
	}
	private void searchAndEdit()
	{
		int pick = 0;
		do
		{
			pick = this.searchMenu.showMenuGetChoice("Enter the search method you wish to use: ");
			if(pick == 1)
			{
				//search by artist;
			}
			else if(pick ==2)
			{
				//search by album
			}
			//add id options if we go there...
		}while(pick != this.searchMenu.getIndexOfExitOption());
		//syso search by
				//if name - method()
				//if album -()
		//choice = search type
		//search result has options to pass into edit or delete function?
		//back
	}
	private void deleteStuff()
	{
		int pick =this.deleteMenu.showMenuGetChoice("Enter the deletion method you wish to use: ");
		do
		{
			if(pick==1)
			{
				//search by artist name
				//need additional logic for displaying and selecting from mult entries
			}
			else if(pick==2)
			{
				//search by album name
			}
			else if(pick ==3)
			{
				//delete file/whole record
			}
			//add in delete by id's
		}while(pick!=this.deleteMenu.getIndexOfExitOption());
		//syso delete
		//delete by album ID
		//delete by artist ID??
		//delete file/all
		//back
	}
	private void editSettings()
	{
		//syso settings
		//set path
		//set filename
		//back
	}
	private void startMenus()
	{
		//main
		this.mainMenu = new MenuHandler("Main Menu", 5);
		this.mainMenu.add("Create Album Record");
		this.mainMenu.add("Search for Album Records");
		this.mainMenu.add("Delete Records");
		this.mainMenu.add("Settings");
		//create
		this.createMenu = new MenuHandler("CreateMenu", 0);//might not need this
		//search
		this.searchMenu = new MenuHandler("Search Menu", 3);//or 5
		this.searchMenu.add("Search By Artist Name");
		this.searchMenu.add("Search By Album Name");
		//this.searchMenu.add("Search By Artist ID");
		//this.searchMenu.add("Search By Album ID");
		this.deleteMenu = new MenuHandler("Delete", 4);//or 6
		this.deleteMenu.add("Delete Record By Artist Name");
		this.deleteMenu.add("Delete Record By Album Name");
		this.deleteMenu.add("Delete Record By Artist ID");
		this.deleteMenu.add("Delete Record By Album ID");
		this.deleteMenu.add("Delete All Records");
	}
}