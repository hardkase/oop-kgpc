package controller;
//Handles File, Path, and FileNames
import java.io.File;
import utility.StringUtility;
import model.Record;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import model.Record;
import utility.AppData;

public class RecordManager
{
	private String path, fName, perm;
	private File file;
	private RandomAccessFile raf;
	private String padStr = AppData.DefaultPadString;
	
	public RecordManager(String path, String fName, String perm)
	{
		this.setPath(path);
		this.setName(fName);
		this.setPerm(perm);
		File folder = new File (this.path);
		if (!folder.exists())
			folder.mkdirs();
	}
	private void setPath(String path)
	{
		this.path = validate(path, AppData.RegexFileName, AppData.DefaultPath );
	}
	private void setName(String strName)
	{
		this.fName = validate(fName, AppData.RegexFileName, AppData.DefaultFileName );
	}
	private void setPerm(String perm)
	{
		this.perm = validate(perm, AppData.RegexFileName, AppData.DefaultFilePerm);
	}
	public String getPath()
	{
		return path;
	}
	public String getFName()
	{
		return fName;
	}
	public String getPerm()
	{
		return perm;
	}
	public static String validate(String strData, String strRegex, String strDefault )
	{
		strData = strData.toLowerCase();
		if (strData.matches(strRegex))
			return strData;
		else
			return strDefault;
	}
	public boolean open(boolean bAppend)
	{
		this.file = new File(this.path + this.fName);
		if(!bAppend)
			this.file.delete();
		try
		{
			this.raf=new RandomAccessFile(this.file, this.perm);
		}
		catch (FileNotFoundException e)
		{
			return false;
		}
		return false;
	}
	public boolean close()
	{
		try
		{
			this.raf.close();
		}
		catch (IOException e)
		{
			return false;
		}
		this.raf = null;
		this.file = null;
		return true;
	}
	//ugh repeat for used data types...
	public void writeBoolean(boolean data)
	{
		writeBoolean(-1, data);
	}
	public void writeBoolean(int index, boolean data)
	{
		setPointer(index);
		try
		{
			this.raf.writeBoolean(data);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public void writeByte(byte data)
	{
		writeByte(-1, data);
	}
	public void writeByte(int index, byte data)
	{
		setPointer(index);
		try
		{
			this.raf.writeByte(data);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public void writeInt(int data)
	{
		writeInt(-1, data);
	}
	public void writeInt(int index, int data)
	{
		setPointer(index);
		try
		{
			this.raf.writeInt(data);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public void writeUTF(String data)
	{
		writeUTF(-1, data);
	}
	public void writeUTF(int index, String data)
	{
		setPointer(index);
		try
		{
			this.raf.writeUTF(data);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public void writeRecord(Record record)
	{
		writeRecord(-1, record);
	}
	public void writeRecord(int index, Record record)
	{
		setPointer(index);
		try
		{
			raf.writeInt(record.getAlbumId());
			raf.writeInt(record.getArtistId());
			String arNa=StringUtility.pad(record.getArtistName(),StringUtility.StandardPadLength, padStr);
			raf.writeUTF(arNa);
			String alNa=StringUtility.pad(record.getAlbumName(),StringUtility.StandardPadLength, padStr);
			raf.writeUTF(alNa);
			raf.writeByte(record.getMusicType());
			raf.writeByte(record.getEra());
			raf.writeBoolean(record.isClassic());
			raf.writeByte(record.getUserRating());
			raf.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public boolean readBool(int index)
	{
		if((index<0) || (index >= size()))
			return false;
		setPointer(index);
		boolean data = false;
		try
		{
			data = this.raf.readBoolean();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return data;
	}
	public byte readByte(int index)
	{
		if((index<0) || (index >= size()))
			return -1;
		setPointer(index);
		byte data = -1;
		try
		{
			data = this.raf.readByte();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return data;
	}
	public int readInt(int index)
	{
		if((index<0) || (index >= size()))
			return -1;
		setPointer(index);
		int data = -1;
		try
		{
			data = this.raf.readInt();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return data;
	}
	public String readString(int index)
	{
		if((index<0) || (index >= size()))
			return null;
		setPointer(index);
		String data = null;
		String padStr=AppData.DefaultPadString;
		String strOut=null;
		try
		{
			data = this.raf.readUTF();
			strOut = StringUtility.unpad(data, padStr);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return strOut;
	}
	public Record readRecord(int index) throws IOException
	{
		if((index<0)||(index>=size()))
			return null;
		setPointer(index);
		int albumId=raf.readInt();
		int artistId=raf.readInt();
		String artistName = StringUtility.
				unpad(raf.readUTF(), padStr);
		String albumName = StringUtility.
				unpad(raf.readUTF(), padStr);
		byte musicType=raf.readByte();
		byte era = raf.readByte();
		boolean isClassic = raf.readBoolean();
		byte userRating = raf.readByte();
		Record r1 = new Record(albumId, artistId, artistName,
				albumName, musicType, era, isClassic, userRating);
		return r1;
	}
	public void setPointer(int index)
	{
		try
		{
			int Record.Size; //hardcoded, probably not a good thing<inflexible>
			if(index == 0)
			{
				this.raf.seek(0);
			}
			else if ((index >0) && (index < size()))
				this.raf.seek(index*recordSize);
			else
			{
				this.raf.seek(this.raf.length());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public int size()
	{
		int recordSize = Record.Size; //(hardcoded?? - can I use Size from record???)
		int size = 0;
		try
		{
			size = (int)(this.raf.length()/recordSize);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return size;
	}
	public void deleteLast() throws IOException
	{
		//long rWpointer = raf.getFilePointer();
		int recordSize=Record.Size;
		raf.setLength(raf.length()-recordSize);
		raf.close();
	}
	public void deleteAt(int index) throws IOException
	{
		int recordSize=Record.Size;
		int deleteIndex=index;
		int deletePt=(deleteIndex*recordSize);
		raf.seek(0);
		raf.seek(raf.length()-recordSize);
		long dIndex=raf.getFilePointer();
		Record r1 = readRecord((int)dIndex);
		raf.seek(0);
		writeRecord(deletePt, r1);
		raf.setLength(raf.length()-recordSize);
		raf.close();
	}
	public void deleteAll() 
	{
		
	}
	
	// ADD print, find, edit, delete, deleteAll..:-)
}