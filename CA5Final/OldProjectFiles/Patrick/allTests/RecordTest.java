package allTests;
import model.Record;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.junit.Test;

import model.Record;

public class RecordTest
{
/*
 * 		this.albumId=albumId;
		this.artistId=artistId;
		this.artistName=artistName;
		this.albumName=albumName;
		this.pMusicType=pMusicType;
		this.era=era;
		this.isClassic=isClassic;
		this.userRating=userRating;
		***need legend for int codes***
 */
	@Test
	public void testNewRecord() throws IOException
	{
		RandomAccessFile raf = new RandomAccessFile("f:/temp/recTest.txt", "rw");
		
		Record test = new Record(001, 0015, "prince", "purple rain",  
			(byte)2, (byte)5, true, (byte)3);
		assertNotNull(test);
		boolean success = test.write(raf);
		assertTrue(success);
		boolean bExists = test.exists();
		assertTrue(bExists);
		raf.close();
	}
	@Test
	public void testReadRecord() throws IOException
	{
		RandomAccessFile raf = new RandomAccessFile("f:/temp/writeRecordTest.txt", "rw");
		Record r = new Record (001, 0015, "prince", "purple rain",  
				(byte)2, (byte)5, true, (byte)3);
		boolean success = r.write(raf);
		assertTrue(success);
		raf.seek(0);
		Record rIn = Record.read(raf);
		assertEquals(rIn.getAlbumId(), 001);
		assertEquals(rIn.getArtistName(), "prince");
		assertEquals(rIn.getAlbumName(), "purple rain");
		assertEquals(rIn.getMusicType(), 2);
		assertEquals(rIn.getEra(), 5);
		assertEquals(rIn.isClassic(), true);
		assertEquals(rIn.getUserRating(), 3);
		raf.close();	
	}

}
