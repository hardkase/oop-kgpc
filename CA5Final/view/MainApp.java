package view;
//KGPC's
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import utility.StringUtility;
import utility.MenuHandler;
//import utility.PrintHandler;
import utility.ScannerUtility;
import utility.AppData;
import controller.RecordManager;
import model.Record; 
/*OOP CA 5
 * 2GD1:author @ Kayleigh Grumley & Patrick Collins
 * Code heavily referenced NMCG's sample code, 
 * Thanks, Niall!
 * Version 0.5 - Getting Started
 * Bitbucket repo: 
 * 2GD1:author @ Kayleigh Grumley & Patrick Collins
 * Code heavily referenced NMCG's sample code, 
 * Thanks, Niall!
 * Version 0.6 - Base classes built out, AppData, Main, and Menu need to be built out, RecordManager
 * needs additional methods
 * OLDVersion 0.5 - Getting Started
 * Bitbucket repo: https://hardkase@bitbucket.org/hardkase/oop-kgpc.git
 */

/*REFERENCE
 * data type sizes
 * BOOLEAN: 1 bit (0 or 1)
 * BYTE: 1 BYTE, 8 bits (127:-128)
 * SHORT: 2 BYTES, 16 bits (32767:-32768)
 * INT: 4 BYTES, 32 bits (2Billion+-)
 * LONG: 8 BYTES, 64 bits(9Quintillion+-int)
 * FLOAT: 4 BYTES, 32 bits(not precise)
 * DOUBLE: 8 BYTES, 64 bits(precise)
 * CHAR: 2 BYTES, 16 bit (i.e. 'a')
 * Our Record:
 * Int + Int + (big) String + (big) String
 * +byte+byte+bool+byte
 * 4+4+100+100+1+1+1+1= 212 Bytes
 * RecordSize=212 Bytes
 * Intra Record Index
 * [0]=AlbumId [4]=ArtistId [8]=ArtistName
 * [108]=albumName [208]=musicType [209]=era 
 * [210]=isClassic[211]=userRating;
 */
public class MainApp
{
	public static void main(String[] args) throws IOException
	{
		MainApp theApp =new MainApp();
		theApp.start();
	}
private MenuHandler mainMenu, editSettingsMenu,  deleteMenu, searchAndEdit;
private String path = AppData.DefaultDriveLetter+AppData.DefaultFileName;
private String fName = AppData.DefaultFileName;
private String perm = AppData.DefaultFilePerm;
private String padString = AppData.DefaultPadString;
	public void start() throws IOException
	{
		System.out.println("Welcome messagey thing");
		//
		startMenus();
		showMain();
	}
	private void showMain() throws IOException
	{
		int pick = 0; //choice
		do
		{
			//open or create database
			pick = this.mainMenu.showMenuGetChoice("Pick one");
			if (pick ==1)//Show All Records
			{
				printRecords(path, fName, perm);
			}
			else if (pick==2)//Search By...
			{
				searchAndEdit();
			}
			else if (pick == 3)//Delete
			{
				addRecord(path, fName, perm);
			}//search&delete, delete @ index, delete all
			/*else if//Settings
			{
				//editSettings();//change drive, path, and file names
			}*/
			else if (pick ==4)
			{
				//addRecord(path, fName, perm);
				deleteMenu();
			}
			else
			{
				editSettings();
			}
				
		}while(pick != this.mainMenu.getIndexOfExitOption());
		System.out.println("See you later!");
	}
	/*public void intializeDB() throws IOException 
	{
		int pick = ScannerUtility.getInt("Would you like to"
				+"1> Create a new database OR"
				+"2> Use an existing database?" );
		if(pick==1)
			openDB();
		else
			createLoadDB();
	}*/
	private void addRecord(String path, String fName, String perm) throws IOException 
	{
		RecordManager rm = new RecordManager(path, fName, perm);
		rm.open(true);
		int lastAlbumId = rm.getLastRecordAlbumId();
		int albumid = lastAlbumId + 1;
		int artistid = ScannerUtility.getInt("Please enter Artists id : ");
		String artist = ScannerUtility.getString("Please enter artist name");
		String titleAlbum = ScannerUtility.getString("Please enter album name ");
		System.out.println("Hip Hop =1, Rock = 2, Indie = 3, R&B = 4, Metal = 5, KPOP = 6, Jazz = 7, No Genre = 0");
		byte pmusic = ScannerUtility.getByte("Please enter primary genre :");
		byte smusic = ScannerUtility.getByte("Please enter secondary genre if any :");
		byte era = ScannerUtility
				.getByte("Please enter Era of music e.g. 90's,80's etc. Note! only put in the number e.g. 90 ");
		int release = ScannerUtility.getInt("Please enter release date ");
		String classic = ScannerUtility.getString("is this album a classic? Please type yes/no ");
		boolean classicB;
		if (classic.equals("yes")) 
		{
			classicB = true;
		} else 
		{
			classicB = false;
		}
		rm.write(new Record(albumid, artistid, artist, titleAlbum, pmusic, smusic, era, release, classicB), -1);

		System.out.println("Record size after addition : " + rm.size());
		rm.close();
	}
	
	/*private void createLoadDB() throws IOException
	{
		int pick =0;
		do
			{
				pick = ScannerUtility.getInt("1: Open Existing DB \n 2: Create New DB");
				if(pick==1)
				{
					openDB();
				}
				else
				{
					createNewDB();
				}
			}while(pick != this.createLoadDB.getIndexOfExitOption());	
	}
	public void openDB() throws FileNotFoundException
	{
		
		try
		{
		 String uPath = ScannerUtility.getString("Please enter the file path to your DB: ");
		 RecordManager.validate(uPath, AppData.RegexFilePath, AppData.DefaultFilePath);
		 path=uPath;
		 String uFname= ScannerUtility.getString("Please enter the name of your DB file: ");
		 RecordManager.validate(uFname, AppData.RegexFileName, AppData.DefaultFileName);
		 fName=uFname;
		 boolean exists=RecordManager.ifExists(uPath, uFname, perm);//validate inserts defaults, so shouldn't need loop PC
		 //so we've specified filepath/name but we're not doing anything here? Think exists method opens file...
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		 /*while(!exists)
			{
				 uPath = ScannerUtility.getString("Database Doesn't exist! Please enter name of database you would like to open : ");
				 uFname=name+".txt";
				 exists=RecordManager.ifExists(path, fName, perm);
			}
	}*/
	private void createNewDB()
	{
		try
		{
			 String uPath = ScannerUtility.getString("Enter the file path of the new DB: ");
			 RecordManager.validate(uPath, AppData.RegexFilePath, AppData.DefaultFilePath);
			 path=uPath;
			 String uFname = ScannerUtility.getString("Enter the filename for your database");
			 RecordManager.validate(uFname, AppData.RegexFileName, AppData.DefaultFileName);
			 fName = uFname;
			 boolean exists=RecordManager.ifExists(path, fName, perm);
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	private void searchAndEdit() throws IOException
	{
		int pick = 0;
		do
		{
			/*private void searchBy(String strPath, String strName, String strPer) throws IOException 
			{*/
			RecordManager rm = new RecordManager(path, fName, perm);
			rm.open(true);
			System.out.println();
			System.out.println(
					"Artist ID = 0, Artist Name = 1, Album name = 2, Genre music = 3, Era = 4, Released Date = 5, Classic or not = 6");
			int search = ScannerUtility.getInt("What would You like to search by? ");
			if (search == 1 || search == 2) 
			{
				String name = ScannerUtility.getString("Please enter what you would like to search for :");
				ArrayList<Record> filteredStringList = rm.filterByString(name,search);
				printArray(filteredStringList);
			}
			else if(search == 3 || search == 4)
			{
				if(search==3)
				{
					System.out.println("Hip Hop =1, Rock = 2, Indie = 3, R&B = 4, Metal = 5, KPOP = 6, Jazz = 7, No Genre = 0");
				}
				byte genreOrEra = ScannerUtility.getByte("Please enter what you would like to search for :");
				ArrayList<Record> filteredStringList = rm.filterByByte(genreOrEra,search);
				printArray(filteredStringList);
			}
			else if (search == 0 || search == 5) {

				int iDorReleaseDate = ScannerUtility.getInt("Please enter what you would like to search for :");
				ArrayList<Record> filteredStringList = rm.filterByInt(iDorReleaseDate,search);
				printArray(filteredStringList);
			}
			rm.close();
		}while(pick != this.searchAndEdit.getIndexOfExitOption());
		//syso search by
				//if name - method()
				//if album -()
		//choice = search type
		//search result has options to pass into edit or delete function?
		//back
	}
	private void deleteMenu() throws IOException
	{
		int pick =this.deleteMenu.showMenuGetChoice("Enter the deletion method you wish to use: ");
		do
		{
			if(pick==1)
			{
				 deleteRecordFromPos(path, fName, perm);
			}
			else if(pick==2)
			{
				deleteAllRecords(path, fName, perm);
			}
			/*else if(pick ==3)
			{
				//delete file/whole record
			}
			//add in delete by id's
			else if(pick==4)
			{
				
			}
			else if(pick==5)
			{
			
			}
			else
			{
				
			}*/
		}while(pick!=this.deleteMenu.getIndexOfExitOption());
		//syso delete
		//delete by album ID
		//delete by artist ID??
		//delete file/all
		//back
	}
private void editSettings()
	{
		int pick=0;
		do
			{
				pick = ScannerUtility.getInt("1: Edit File Path \n 2: Edit File Name");
				if (pick == 1)
				{
					editFilePath();
				}
				else
				{
					editFileName();
				}
			}while(pick!=this.editSettingsMenu.getIndexOfExitOption());
	}
	public void editFilePath()
	{
		System.out.println("Current Filepath is: " +path);
		String newPath=ScannerUtility.getString("Directory Path to Save Record to: ");
		RecordManager.validate(newPath, AppData.RegexFilePath, AppData.DefaultFilePath);
		String path = newPath;
		
	}
	public void editFileName()
	{
		System.out.println("Current filename is: "+fName);
		String newFname=ScannerUtility.getString("New File Name: ");
		RecordManager.validate(newFname, AppData.RegexFileName, AppData.DefaultFileName);
		String fName = newFname;
	}
	private void startMenus()
	{
		//main
		this.mainMenu = new MenuHandler("Main Menu", 6);
		this.mainMenu.add("Show All Records");
		this.mainMenu.add("Search and Edit");
		this.mainMenu.add("Create a Record");
		this.mainMenu.add("Delete Records");
		this.mainMenu.add("Edit Settings");
		//this.createLoadDB=new MenuHandler("Create or Load DB", 3);
		//thi//s.createLoadDB.add("Load Existing DB");
		//is.createLoadDB.add("Create New DB");
		//search
		this.searchAndEdit = new MenuHandler("Search and Edit", 1);//or 5
		/*this.searchMenu.add("Search By Artist Name");
		this.searchMenu.add("Search By Album Name");
		this.searchMenu.add("Search By Record Index");
		this.searchMenu.add("Search By Artist ID");
		this.searchMenu.add("Search By Album ID");*/
		//delete
		this.deleteMenu = new MenuHandler("Delete Records", 2);//or 6
		//this.deleteMenu.add("Delete Record By Index");
		this.deleteMenu.add("Delete All Records");
		///this.deleteMenu.add("Delete By Record Index");
		//this.deleteMenu.add("Delete Record By Artist ID");
		//this.deleteMenu.add("Delete Record By Album ID");
		//this.deleteMenu.add("Delete All Records");
		
		this.editSettingsMenu = new MenuHandler("Edit Settings", 3);
		this.editSettingsMenu.add("Edit File Path where Record is stored");
		this.editSettingsMenu.add("Edit Record File Name");
	}

	//DELETE ALL 
		private void deleteAllRecords(String strPath, String strName, String strPer) throws IOException {
			RecordManager rm = new RecordManager(strPath, strName, strPer);
			rm.open(true);
			int yousure = ScannerUtility.getInt("Are you sure you wish to delete all records? yes=1, no=2 ");
			if (yousure == 1) {
				rm.deleteAllRecords();
			}

			rm.close();
		}
		//DELETE FROM INDEX n
		private void deleteRecordFromPos(String path, String fName, String perm) throws IOException 
		{
			RecordManager rm = new RecordManager(path, fName, perm);
			rm.open(true);
			int index = ScannerUtility.getInt("please enter position of record you would like to delete ");
			rm.deleteAtPoint(index);
			rm.close();
			/*for (Record r : filteredStringList) 
			{
				System.out.println(r.toString());
			}*/
		}
		private void printArray(ArrayList<Record> filteredStringList) 
		{
			for (Record r : filteredStringList) {
				System.out.println(r.toString());
			}
		}
		private void printRecords(String strPath, String strName, String strPer) throws IOException {
			RecordManager rm = new RecordManager(strPath, strName, strPer);

			System.out.println("print and read");
			rm.open(true);
			rm.print();
			rm.close();
		}
}