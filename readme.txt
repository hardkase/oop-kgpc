REGURGITATION OF CA5 PROJECT SPEC
Includes thoughts about and ideas to improve end product for more points. Optional bits and thoughts in BLUE
02/05/2016 - Upload current bones: record& string utility built, record manager base built, needs extra methods, imported NMCG's scannerUtility, still needs appdata, menu, main, and tests
01/05/2016 - Upload "bones" contains starting files. Only Record.java built out. V.5
-Patrick
CA 5 main points
	flat file/database app using RandomAccessFile 'raf'.
Needs to:
	Create: create a new database file. When specifying file name, check that it doesn't already exist, give user chance to choose new name
	Open: Open an existing database by entering a valid file name
	Delete: Delete a db file specified by user
Should:
	Add: add a new record to the database. Detect and prevent duplicates
	Edit: Edit any field of a given record in the database
	Search: Display details of all records matching user specified criteria (name, id, year, etc)
	Delete: Delete a user specified record in the database
	Delete All: drop all records from database
	Count - Display total number of records in Database
Each record must contain at least 6 fields. At least 2, max 3 fields can be strings. Others can be float, int, double, boolean, etc. I don't think objects/enums will work.
CMD line GUI in DOS
TESTING - tests for all major functional requirement methods. Should create tests for all sequences of operations (create open add delete search <edit>)
Interview - we know how this goes
Packaging & Deployment
	Source files
	JAR file (needs to run)
	include test data files?
SHINY EXTRA BITS
	Output to multiple file types (CSV, XML, HTML)  need to determine how to do char stream stuff from raf file. Should be do-able. NMCG provided sample of this last class.
	Really slick UI. NMCG says DOS style, so think using jpanel not a good idea. Just real slick and solid command line GUI should do the job. Will try to implement his menu options stuff.
	Really good REGEX validation. Stuff like taking input and Upper Casing first words with an exception list for certain words (on, in, the, of, and, etc).
30/04/2016 - Added file CA5bones to DB, I think this has all the needed classes to rock this shit
KG  if you have additional thoughts, fire them below in a diff color



